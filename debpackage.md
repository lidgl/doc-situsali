# Membangun Paket Kernel pada Distro Debian.

## Langkah-langkah.

### 1. Mendapatkan kernel source. 

```
	$ wget -c https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.5.5.tar.xz
```
<br>

### 2.  Extrak kernel.

```
	$ tar -xf linux-6.5.5.tar.xz
	$ cd linux-6.5.5
```
<br>

### 3. Mengkonfigurasi kernel. 

#### 3.0 Salin dan simpan konfigurasi.

```
	$ cp /boot/config-$(uname -r) .config 
	$ make menuconfig
```
<br>

#### 3.1 Disable system keys.

```
	$ scripts/config --disable SYSTEM_TRUSTED_KEYS
	$ scripts/config --disable SYSTEM_REVOCATION_KEYS
```
<br>

#### 3.2 Skip debugging.

```
	$ scripts/config --disable DEBUG_INFO
	$ scripts/config --enable DEBUG_INFO_NONE
```
<br>

#### 3.3 Disable system trusted keys pada konfig.

Tambahkan baris ini ke `.config`.
```
	CONFIG_SYSTEM_TRUSTED_KEYS=""
```
<br>

### 4. Membangun paket kernel.

```
	$ nice make -j4 bindeb-pkg
```
<br>

### 5.  Memasang paket kernel.

```
	$ cd ..
	$ sudo dpkg -i *.deb
```
<br>

## Sumber dan referensi.

1. [Sumber 1.](https://www.dwarmstrong.org/kernel/)
1. [Sumber 2.](https://wiki.debian.org/BuildADebianKernelPackage)
