# Mencoba Ubuntu Mantic Minotaur (Development Branch).

Ubuntu Lunar Lobster baru rilis beberapa waktu yang lalu. Pengembangan versi penerusnya pun sudah berjalan. Melalui laman twitter [ubuntu](https://twitter.com/ubuntu), diumumkan bahwa kode nama untuk versi ubuntu selanjutnya adalah _**Mantic Minotaur**_. Perilisannya pun dijadwalkan pada 12 Oktober 2023.

## Versi terbaru, apakah sudah bisa dicoba ?

Untuk saat ini Ubuntu Mantic Minotaur sudah bisa kita coba, walaupun belum ada ISO resmi. Jika kita sudah memasang Ubuntu versi sebelumnya (23.04), kita tinggal mengupdate dan mengupgrade saja. Proses detailnya akan kita paparkan pada paragraf selanjutnya. Prosesnya cukup sederhana dan mudah diikuti.

## Persiapan dan Langkah-Langkah.

### Persiapan.

Berikut ini adalah beberapa hal yang perlu kamu persiapkan sebelum melakukan update dan upgrade :

- Editor teks, kita gunakan untuk mengedit berkas sources.list.
- Koneksi internet, kita butuh koneksi internet untuk mengunduh update dan upgrade.
- Cadangan / _Backup_ (opsional), untuk berjaga - jaga kalau ada kegagalan.

### Langkah - Langkah.

Setelah persiapan sudah selesai, kita bisa langsung masuk pada langkah - langkah untuk update dan upgrade. Berikut ini adalah langkah - langkah yang bisa kamu lakukan untuk mengupdate dan mengupgrade Ubuntu Lunar Lobster menjadi Ubuntu Mantic Minotaur :

- mengedit sources.list.

Pada langkah ini, kita akan mengubah isi berkas sources.list

- update, upgrade.

Setelah berkas sources.list sudah disesuaikan, kita hanya perlu menjalankan perintah-perintah untuk update dan upgrade. Berikut adalah perintah - perintahnya :

```
$ sudo apt update
$ sudo apt upgrade
$ sudo apt full-upgrade
$ sudo apt dist-upgrade
```
- reboot